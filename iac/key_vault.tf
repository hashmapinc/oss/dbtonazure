
resource "azurerm_key_vault" "kv" {
  name = join("", [
    local.name_prefix,
    "kv01"
  ])

  tags = var.tags_base

  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  sku_name  = "standard"
  tenant_id = data.azurerm_client_config.current.tenant_id
}

# The deployment SP should have set permission to write the 
# secrets. ensure that the access policy is defined for this
resource "azurerm_key_vault_access_policy" "kv_acp_deployer" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  secret_permissions = ["get", "list", "set", "delete"]
}

# Store the storage account SAS key, this will be used 
# to copy the project files into the docker image
#
resource "azurerm_key_vault_secret" "kvs_sa_sas" {
  depends_on   = [azurerm_key_vault_access_policy.kv_acp_deployer]
  name         = "sa-sas"
  value        = azurerm_storage_account.adls2.primary_access_key
  content_type = "SAS key for accessing the storage account, which holds the dbt code."
  key_vault_id = azurerm_key_vault.kv.id
  tags         = var.tags_base
}

# Store the service principal information which will
# enable to deploy aci on the fly.
resource "azurerm_key_vault_secret" "kvs_acr_sp" {
  depends_on   = [azurerm_key_vault_access_policy.kv_acp_deployer]
  name         = "acr-sp"
  value        = data.azurerm_client_config.current.client_id
  content_type = "The service principal which will create aci"
  key_vault_id = azurerm_key_vault.kv.id
  tags         = var.tags_base
}

resource "azurerm_key_vault_secret" "kvs_acr_sp_scrt" {
  depends_on   = [azurerm_key_vault_access_policy.kv_acp_deployer]
  name         = "acr-sp-scrt"
  value        = var.spsecret
  key_vault_id = azurerm_key_vault.kv.id
  content_type = "The secret of service principal which will create aci"
  tags         = var.tags_base
}

resource "azurerm_key_vault_secret" "kvs_sflk_conn" {
  depends_on   = [azurerm_key_vault_access_policy.kv_acp_deployer]
  name         = "sflk-conn"
  value        = <<SCRT_EOF
		{ 
       'SNOWSQL_ACCOUNT': 'abc.us-east-1', 
       'SNOWSQL_USER': 'SOMEBODY', 
       'DBT_PASSWORD': 'abracadabra', 
       'SNOWSQL_ROLE': 'PUBLIC', 
       'SNOWSQL_DATABASE': 'DEMO_DB', 
       'SNOWSQL_WAREHOUSE': 'DEMO_WH' 
     }
	SCRT_EOF
  content_type = "snowflake connection info"
  key_vault_id = azurerm_key_vault.kv.id
  tags         = var.tags_base
}
