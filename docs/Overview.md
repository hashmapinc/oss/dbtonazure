# Overview

[[_TOC_]]

## Docs
 The following are subject specific documentation:
  - [Installation](./Installation.md)
  - [Resources](./Resources.md)

## Acronyms
 - ACI : Azure container instance
 - ACG : Azure container group
 - ACR : Azure container registry
 - ADF : Azure data factory

## Implementation 
This reference prototype installation when deployed would create different resources as below:

![](./images/dbtonaz_rg.png)


## Demo execution - ACG
In the below video walk through, i am showing how to start the Azure container group, which will instantiate the Azure container instance. Once instantiated, I highlight : 
  - The environment variables that are configured with appropriate values.
  - The DBT pipeline script, for now it is just a dbt list command, that gets called and you can see the output.
  - I also print out the dummy snowflake connection info, which gets retrieved from
  the key vault.

[Video : ./videos/demo_execution.mp4](./videos/demo_execution.mp4)

Here is a screenshot highlighting the output of the container log, after dbt pipeline is executed.

![](./images/docker_execution_log.png)

## Demo execution - ADF
In the below video walk through, i am demonstrating the ADF triggering the ACG. Once instantiated, I highlight : 
 - The service principal secrets that gets stored in the key vault
 - The Key vault linked service in the ADF
 - The pre-configured pipeline which is used to trigger the ACG
 - Show that the ACG was in a stopped state
 - Trigger, manually, the pipeline.
 - The pipeline triggers the ACG and waits 
 - ACG starts and performs its actions.
 - Once completed, show the log output of ACG and also the status in ADF.

[Video : ./videos/demo_execution_adf_tigger.mp4](./videos/demo_execution_adf_tigger.mp4)

NOTE: With the current simulation set to 5 min, i had to temporarily pause the recording and start it back once completed.

## Source code directories

| Directory | Comment |
| :---------- | :--- |
| dbtdataops/dbtoncloud | A sample dbt project. |
| dbtdocker | The artifacts required to build the docker image. |
| iac | Terraform scripts that is used to deploy. |
